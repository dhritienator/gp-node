const express = require('express');
const router = express.Router();
const db = require('../util/db')
const UserLogin = require('../middleware/requireAdmin');
const jwt = require('jsonwebtoken');
const { JWT_KEYS } = require('../util/keys')
router.post("/login", (req, res) => {

    var { email, password } = req.body
    if (!email || !password) {
        return res.status(422).json({ status: "fail", message: "Please provide all required details" })
    }

    db.execute("SELECT * FROM admin WHERE adminEmail = ? AND password = ?",[email, password])
        .then(([respdata]) => {
            if (!respdata[0]) {
                return res.json({ status: "fail", message: "Invalid User" })
            }
            else {
                var userid = respdata[0].adminId
                const token = jwt.sign({ id: userid }, JWT_KEYS)
                console.log('userid', userid)
                res.json({ status: "success", message: "Login successfully", token: token })
            }


        })


        .catch((err) => {

        })

})
router.get('/', UserLogin, (req, res) => {
    res.json(req.user)
})

module.exports = router;