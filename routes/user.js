const express = require('express');
const router = express.Router();
const db = require('../util/db')
const UserLogin = require('../middleware/requireAdmin');
const jwt = require('jsonwebtoken');
const { JWT_KEYS } = require('../util/keys');
const { SERVER_URL } = require('../util/config');
const multer = require('multer')
const path = require("path");
const fs=require('fs');
var mkdirp = require('mkdirp');
const { request } = require('http');
const serviceimagepath = "./assets/image/service/"
const folderpath = "./assets/"
const servicestorage = multer.diskStorage({
    destination: serviceimagepath,
    filename: (req, file, cb) => {
        return cb(null, `${file.fieldname}_${Date.now()}${path.extname(file.originalname)}`)
    }
})

const uploadserviceimg = multer({
    storage: servicestorage,
    limits: {
        fileSize: 1024 * 1024
    }
})

router.post('/InsertNewService', UserLogin, uploadserviceimg.single('avatar'), (req, res, next) => {
    var fileName = req.file.filename;
 
    var {name, desc1, desc2, desc3, desc4, sStatus} = req.body

    if(!name || !desc1){
        return res.status(422).json({ status: "fail", message: "Please provide all details"})
    }

    db.execute("INSERT INTO service (serviceImage, serviceName, serviceStatus,serviceDesc1,serviceDesc2, serviceDesc3, serviceDesc4) VALUES (?,?,?,?,?, ?, ?)", [fileName, name, sStatus,desc1, desc2, desc3, desc4])
    .then(([respdata]) => {
        console.log("success")
        res.json({ status: "success", message: "Insert Successful." })
    })
    .catch((err) => {
        console.log("Error in inserting service in table", err);
    })

    //   fs.unlink(destination+savedData[0].profileimg, (err) => {
    //     if (err) throw err;
    //     console.log(destination+'Photo image was deleted');
    // });
    // req.file is the `avatar` file
    // req.body will hold the text fields, if there were any
})
router.post('/UpdateService',UserLogin, uploadserviceimg.single('avatar'), (req, res, next) => {
    var fileName = req.file.filename;

    var {name, desc1, desc2, desc3, desc4, status, serviceid} = req.body
    if(!name || !desc1 || !serviceid){
        return  res.json({ status: "fail", message: "Please provide all details"})
    }
    db.execute('SELECT * FROM service WHERE serviceId = ?',[serviceid])
    .then(([sdata]) => {

        if(fileName !== ""){
            if (sdata[0].serviceImage != "") {
                fs.unlink(serviceimagepath + sdata[0].serviceImage, (err) => {
                    if (err) throw err;
                    console.log(serviceimagepath + 'Service image was deleted');
                });
            }
    
            db.execute("update service SET serviceImage = ?, serviceName = ?, serviceStatus = ?,serviceDesc1 = ?,serviceDesc2 = ?, serviceDesc3 = ?, serviceDesc4 = ? WHERE serviceId = ?", [fileName, name, status, desc1, desc2, desc3, desc4, serviceid])
            .then(([respdata]) => {
                res.json({ status: "success", message: "Service updated with Image Successful." })
            })
            .catch((err) => {
                console.log("Error in updating service table", err);
            })
        } else {
            db.execute("update service SET serviceName = ?, serviceStatus = ?,serviceDesc1 = ?,serviceDesc2 = ?, serviceDesc3 = ?, serviceDesc4 = ? WHERE serviceId = ?", [name, status, desc1, desc2, desc3, desc4, serviceid])
            .then(([respdata]) => {
                res.json({ status: "success", message: "Service updated Successful." })
            })
            .catch((err) => {
                console.log("Error in updating service table", err);
            })
        }

    })
    .catch((err) => {
        console.log('Error in fetching service table data', err)
    })

})

router.get('/GetServiceByID/:id', UserLogin, (req, res) => {
    var  id  = req.params.id;
    db.execute("select * from service where serviceId = ?",[id])
    .then(([respdata]) => {
        if (!respdata[0]) {
            return res.json({ status: "fail", message: "No data found" })
        }
            var Image_Name = respdata[0].serviceImage !== "" ? `${SERVER_URL}service/${respdata[0].serviceImage}` : ""
        // var Image_Name = respdata[0].serviceImage !== "" ? `${respdata[0].serviceImage}` : ""
        output = {
            serviceId       : respdata[0].serviceId,
            serviceName     : respdata[0].serviceName,
            serviceDescription1: respdata[0].serviceDesc1,
            serviceDescription2: respdata[0].serviceDesc2,
            serviceDescription3: respdata[0].serviceDesc3,
            serviceDescription4: respdata[0].serviceDesc4,
            Image_path: Image_Name,
            Service_status: respdata[0].serviceStatus
        }

        return res.json({ status: "success", message: "data found", data: output })
    })
    .catch((err) => {
        console.log("Error in fetching service table", err);
    })
})
router.get('/GetAllService', UserLogin, (req, res) => {
  
    db.execute("select * from service")
    .then(([respdata]) => {
        if (!respdata[0]) {
            return res.json({ status: "fail", message: "No data found" })
        }
        const outputdata = respdata.map((item, index) => {
            // var Image_path = item.serviceImage !== "" ? `${SERVER_URL}category/${item.serviceImage}` : ""
            var Image_Name = item.serviceImage !== "" ? `${item.serviceImage}` : ""
            return (
                output = {
                    serviceId: item.serviceId,
                    serviceName: item.serviceName,
                    serviceDescription1: item.serviceDesc1,
                    serviceDescription2: item.serviceDesc2,
                    serviceDescription3: item.serviceDesc3,
                    serviceDescription4: item.serviceDesc4,
                    Image_path: Image_Name
                }
            )
        })
        return res.json({ status: "success", message: "data found", data: outputdata })
    })
    .catch((err) => {
        console.log("Error in fetching service table", err);
    })
})
router.post('/CreateDirectoryForPortfolio', UserLogin, uploadserviceimg.single('avatar'), (req, res, next) => {
    var fileName = req.file.filename;
 
    var {Servicename, sStatus} = req.body

    if(!fileName || !Servicename){
        return res.status(422).json({ status: "fail", message: "Please provide all details"})
    }

    db.execute("INSERT INTO portfoliodir (pdCoverImg, pdName, pdStatus) VALUES (?,?,?)", [fileName,Servicename,sStatus])
    .then(([respdata]) => {
        fs.mkdir(path.join(folderpath, Servicename), (err) => {
            if (err) {
                return console.error(err);
            }
            console.log('Directory created successfully!');
        });
        // mkdirp('./assets/image/'+pdName, function(err) { 

        //     // path exists unless there was an error
        
        // });
        
        console.log("success")
        res.json({ status: "success", message: "Insert Successful." })
    })
    .catch((err) => {
        console.log("Error in inserting service in table", err);
    })

    //   fs.unlink(destination+savedData[0].profileimg, (err) => {
    //     if (err) throw err;
    //     console.log(destination+'Photo image was deleted');
    // });
    // req.file is the `avatar` file
    // req.body will hold the text fields, if there were any
})
router.get('/GetAllProtfolio', UserLogin, (req, res) => {
  
    db.execute("select * from portfoliodir")
    .then(([respdata]) => {
        if (!respdata[0]) {
            return res.json({ status: "fail", message: "No data found" })
        }
        const outputdata = respdata.map((item, index) => {
            // var Image_path = item.serviceImage !== "" ? `${SERVER_URL}category/${item.serviceImage}` : ""
           // var Image_Name = item.serviceImage !== "" ? `${item.serviceImage}` : ""
            return (
                output = {
                    prtfolioId: item.pdId,
                    serviceName: item.pdName,
                    
                   // Image_path: Image_Name
                }
            )
        })
        return res.json({ status: "success", message: "data found", data: outputdata })
    })
    .catch((err) => {
        console.log("Error in fetching service table", err);
    })
})
router.post('/InsertProtfolio', UserLogin, uploadserviceimg.single('avatar'), (req, res, next) => {
    var fileName = req.file.filename;
 
    var {Prtname, sStatus} = req.body

    if(!fileName || !Servicename){
        return res.status(422).json({ status: "fail", message: "Please provide all details"})
    }

    db.execute("INSERT INTO portfolio (portName,portImage portFolder,portOrder, portStatus,Service) VALUES (?,?,?,?,?,?)", [fileName,Servicename,sStatus])
    .then(([respdata]) => {
        mkdirp('./assets/image/'+pdName, function(err) { 

            // path exists unless there was an error
        
        });
        
        console.log("success")
        res.json({ status: "success", message: "Insert Successful." })
    })
    .catch((err) => {
        console.log("Error in inserting service in table", err);
    })

    //   fs.unlink(destination+savedData[0].profileimg, (err) => {
    //     if (err) throw err;
    //     console.log(destination+'Photo image was deleted');
    // });
    // req.file is the `avatar` file
    // req.body will hold the text fields, if there were any
})
function errHandler(err, req, res, next) {
    if (err instanceof multer.MulterError) {
        res.json({
            status: 'fail',
            error: err.code,
            message: err.message
        })
    }
}
router.use(errHandler);
module.exports = router;