const mysql = require('mysql2');
const connection = mysql.createConnection({
  // host: '103.10.235.189',
  // user: 'egpaidin_admin',
  // database: 'egpaidin_in',
  // password: 'egpaidinadmin'
  host: 'localhost',
  user: 'root',
  database: 'intagedesign',
  password: ''
});
connection.connect(function(err) {
  if (err) {
      console.error('Error connecting to MySql Database: ' + err.stack);
      return;
  }
  console.log('Connected as id ' + connection.threadId);
});
module.exports = connection.promise()