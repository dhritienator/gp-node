const express = require('express')
// const bodyParser = require("body-parser");
 const moment = require('moment-timezone');
const app = express()
const PORT = process.env.PORT || 4000;
var router = express.Router();
var cors = require('cors');
//const homeRoute = require('./routes/home')

// parse requests of content-type: application/json
//app.use(bodyParser.json());

// parse requests of content-type: application/x-www-form-urlencoded
//app.use(bodyParser.urlencoded({ extended: true }));

// app.use(express.static('public'));
// app.use('/category', express.static(__dirname + '/assets/category'));

// app.use('/home', homeRoute)
// app.get('/', (req, res) => {
//     res.send('hello Graphicspanda');
// })
// app.listen(PORT, () => {
// console.log('GraphicsPanda App running on port - ', PORT)
// })
// moment().tz("Asia/Calcutta").format();
// process.env.TZ = 'Asia/Calcutta';
// var date = moment().format('DD-MM-YYYY');
// var time = moment().format('hh:mm:ss')
// console.log(date, time)
app.use(cors())
app.use(express.json())
app.use(express.static('public'));

app.use('/service', express.static(__dirname + '/assets/image/service'));

const authUserRoutes = require('./routes/authUser')
const UserRoutes = require('./routes/user')

app.use('/auth', authUserRoutes)
 app.use('/user', UserRoutes)

// app.get('/', (req, res) => {
//   res.header("Access-Control-Allow-Origin", "*");
//   res.send('hello');
// })

app.listen(PORT, () => {
  console.log('karthik node running on port - ', PORT)
})