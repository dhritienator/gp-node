const jwt = require('jsonwebtoken');
const db = require('../util/db')
const {JWT_KEYS} = require('../util/keys')

module.exports = (req, res, next) => {
    const {authorization} = req.headers
    if(!authorization){
        return res.status(422).json({error: "You must be logged in"})
    }
    const token = authorization.replace("Bearer ", "")
    jwt.verify(token, JWT_KEYS, (err, payload) => {
        if(err){
            return res.status(422).json({error: "You must be logged in."})
        }
        const {id} = payload

        db.execute('SELECT * FROM admin WHERE admin.adminId = ?', [id])
        .then(([savedData]) => {
            var adminDetails = {
                id          : savedData[0].adminId,
                name        : savedData[0].adminName,
                email       : savedData[0].adminEmail,
                mobile      : savedData[0].adminContact,
              
            }
            req.admin = adminDetails
            next()
        })
    })
}